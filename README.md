# Estrategia

Risketos Bàsics:

- Capacitat d’elecció, per exemple varies torretes.
- El HUD es crea per BP.
- Us d’esdeveniments.
- Enemics o objectius a on atacar i al fer-ho que morin.
- Enemics que segueixen un camí.
- Mort del jugador i restart

Risketos Opcionals:

- Control de oleadas
- Augment progressiu de dificultat
- Menu inicial amb 2 nivells random
- Economia en Or i Vida del jugador
- Costos per cada tipus de torre
